const express = require('express');
const { Client, Intents } = require('discord.js');
const signale = require('signale');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT;
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_MESSAGES] });

client.login(process.env.DISCORD_TOKEN);

app.listen(PORT, () => {
  signale.success(`Listenning on ${PORT}`);
});

const handlePingReaction = async (reaction, user) => {
  console.log("received reaction for message", reaction.message.content, "from user", user.id);

  const users = await reaction.users.fetch();
  const pingRoleId = "883654506557079603";
  const guild = reaction.message.guild;
  const role = await guild.roles.resolve(pingRoleId);

  if (!role) {
    signale.error('Role ping not found :/');
    return;
  }

  for (let [, user] of users) {
    //signale.log("adding role ping to user", user.id, user.username);
    const guildMember = await guild.members.fetch(user);

    if (guildMember.roles.cache.has(role))
      return;

    signale.log("adding role ping to member", guildMember.id, guildMember.nickname);

    await guildMember.roles.add(role);
  }
}

client.on('ready', async () => {
  signale.success(`${client.user.tag} Is. Ready. To. Greet. 🤖`);

  const littleBigGuild = client.guilds.resolve('777472459841994774');
  const littleBigReactionChannel = littleBigGuild.channels.resolve('883653892481622037');
  const message = await littleBigReactionChannel.messages.fetch('883654295017357313');

  setInterval(async () => {
    await message.fetch();
    message.reactions.cache.forEach(handlePingReaction);
  }, 1000 * 60 * 2);
  //TODO: remove setInterval, use collectors
  //TODO: remove ping role on people without the role
  //const collector = message.createReactionCollector({ filter: (reaction) => reaction.emoji.name === '📢' });
  //collector.on('collect', handlePingReaction);
});

client.on('guildMemberAdd', (member) => {
  const channelToPost = 'blabla';
  const channel = member.guild.channels.cache.find((ch) => ch.name === channelToPost);
  if (!channel) {
    signale.error('Channel not found :/');
    return;
  }
  channel.send(
    `Bienvenue sur le serveur de Little Big ${member} 🥳. Si tu le souhaites, tu peux rejoindre l'association (gratuit et optionnel) en suivant ce lien : https://docs.google.com/forms/u/1/d/e/1FAIpQLSfARqvP12fiCs3JDs-Rrkl9aRGMJ9F2wUrqQgCsjXwJ21PB_w/formResponse`,
  );
});

client.on('message', async (message) => {
  const channelPresentation = "présentation-des-membres";
  const roleMemberName = "membre";

  try {
    if (!message.member || message.member.bot)
      return;

    const roleMember = message.member.guild.roles.cache.find(role => role.name === roleMemberName);

    if (!roleMember) {
      signale.error('Role membre not found :/');
      return;
    }

    if (message.channel.name !== channelPresentation)
      return;

    if (message.member.roles.cache.has(roleMember))
      return;

    await message.member.roles.add(roleMember);
  } catch(e) {
    signale.error(e);
  }
})

