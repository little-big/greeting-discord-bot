FROM node:16-alpine AS builder

WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile


FROM node:16-alpine
COPY --from=builder /app/node_modules ./node_modules
COPY . .

CMD ["yarn", "run", "start"]

